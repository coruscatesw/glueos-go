# GlueOS Go

GlueOS Go contains all source for Go binaries developed to support GlueOS


# Setup

GlueOS Go adheres to the standard go directory heirarchy. This repository should be cloned in ~/go/src, or the appropriate go source directory if you have overridden the default.


# Dependencies

Relies on golang: sudo apt-get install golang (for debian distros)

Dispatcher relies on another fsnotify repository. To pull that down, run the following command.

go get -u github.com/rjeczalik/notify

Dispatcher also utilizes another repo to get access to a thread safe set container

go get gopkg.in/fatih/set.v0


# Compilation

go build (compiles locally into ~/go/src/glueos-go)

or

go install (compiles into ~/go/bin; required when deploying GlueOS)


# Usage

Sample usage:  ./dispatcher --dirs /tmp/nofile,.,./my.cfg --handler ./handle.sh
