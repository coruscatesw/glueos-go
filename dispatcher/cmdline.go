package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

//------------------------------------------------------------------------------
/**
 * @returns 1,2 where
 *    1 = array of directories to monitor
 *    2 = handler script/binary
 */
//------------------------------------------------------------------------------
func parseCommandLine() ([]string, string) {
	dirsPtr := flag.String("dirs", "", "Comma separated list of directories to monitor for changes")
	var handler string
	flag.StringVar(&handler, "handler", "", "Script or binary that will handle file events")

	flag.Parse()

	if flag.NFlag() < 2 {
		flag.Usage()
		os.Exit(1)
	}

	dirs := strings.Split(*dirsPtr, ",")

	fmt.Println("Monitoring directories:", dirs)
	fmt.Println("Handler set to:", handler)

	return dirs, handler
}
