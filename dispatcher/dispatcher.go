package main

import (
	"github.com/rjeczalik/notify"
	"gopkg.in/fatih/set.v0"
)

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
func main() {
	dirs, handler := parseCommandLine()

	filewatcher := FileWatcher{
		Channel:     make(chan notify.EventInfo, 2),
		DispatchSet: set.New(),
		Handler:     handler}
	filewatcher.generateDirWatchList(dirs)
	filewatcher.registerForDirectoryEvents()
	filewatcher.watchForDirectoryEvents()
}
