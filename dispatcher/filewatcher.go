package main

// Ref: https://godoc.org/github.com/rjeczalik/notify

import (
	"fmt"
	"github.com/rjeczalik/notify"
	"gopkg.in/fatih/set.v0"
	"io/ioutil"
	"os"
	"os/exec"
	"time"
)

type FileWatcher struct {
	Channel     chan notify.EventInfo
	DispatchSet *set.Set
	Handler     string
	Periodic    *time.Ticker
	WatchList   []string
}

//------------------------------------------------------------------------------
/**
 * @param dirs = list of directories to be checked for existence
 */
//------------------------------------------------------------------------------
func (fw *FileWatcher) generateDirWatchList(dirs []string) {
	for _, dir := range dirs {
		fileInfo, err := os.Stat(dir)
		if err != nil {
			fmt.Println("Error locating", dir, "; not added to watch list")
		} else {
			if fileInfo.Mode().IsDir() {
				fw.WatchList = append(fw.WatchList, dir)
			} else {
				fmt.Println("Error: ", dir, " is not a directory; not added to watch list")
			}
		}
	}
}

//------------------------------------------------------------------------------
/**
 * Note: notify.Stop(c) must be called at some point after this function when
 *       events should no longer be monitored

 * @param c = buffered channel used to handle file events
 * @param dirs = directories to register for events
 */
//------------------------------------------------------------------------------
func (fw *FileWatcher) registerForDirectoryEvents() {
	for _, dir := range fw.WatchList {
		fw.registerDirectory(dir)
	}

	// now that we're registered, start a ticker (periodic function) to
	// dispatch file events
	fw.Periodic = time.NewTicker(time.Millisecond * 1000)
	go func() {
		for {
			select {
			case <-fw.Periodic.C:
				for _, file := range set.StringSlice(fw.DispatchSet) {
					fmt.Println("Dispatching", file)
					go fw.dispatchDirectoryEvent(file)
				}
				fw.DispatchSet.Clear()
			}
		}
	}()
}

//------------------------------------------------------------------------------
/**
 * This function registers a single directory for events. It also parses all
 * subdirectories (recursively) and registers for their directory events
 */
//------------------------------------------------------------------------------
func (fw *FileWatcher) registerDirectory(dir string) {
	fileInfo, err := os.Stat(dir)
	if err != nil {
		fmt.Println("registerDirectory Error: ", err)
	} else {
		if fileInfo.Mode().IsDir() {
			fmt.Println("Adding dir <", dir, "> to watch list")

			if err := notify.Watch(dir, fw.Channel,
				notify.InModify,
				notify.InCloseWrite,
				notify.InMovedTo,
				notify.InCreate,
				notify.InDelete); err != nil {
				fmt.Println("Fatal error:", err)
				os.Exit(1)
			}

			files, err := ioutil.ReadDir(dir)
			if err == nil {
				for _, f := range files {
					fp := dir + "/" + f.Name()
					fileInfo, err = os.Stat(fp)
					if err == nil && fileInfo.Mode().IsDir() {
						fw.registerDirectory(fp)
					}
				}
			}
		}
	}

}

//------------------------------------------------------------------------------
/**
 * This is a blocking function. It will run forever and listen for events on
 * the watched directories, dispatching events to the defined handler as they
 * occur
 */
//------------------------------------------------------------------------------
func (fw *FileWatcher) watchForDirectoryEvents() {
	defer notify.Stop(fw.Channel)

	for true {
		ei := <-fw.Channel

		//fmt.Println(ei.Event(), " ", ei.Path())

		// if one of our watchdirs is deleted, notify
		if ei.Event() == notify.InDelete {
			for _, dir := range fw.WatchList {
				if ei.Path() == dir {
					//@TODO - this needs to be throttled. We can get multiple events for a delete
					fmt.Println("Warning: watch dir", dir, "deleted. Cannot be monitored until it is recreated and this service is restarted.")
					break
				}
			}
			continue
		}

		// if a new sub dir is created, watch
		if ei.Event() == notify.InCreate {
			fileInfo, err := os.Stat(ei.Path())
			if err != nil {
				fmt.Println("Error: ", err)
			} else {
				if fileInfo.Mode().IsDir() {
					fmt.Println("Subdirectory created under monitored directory")
					fw.registerDirectory(ei.Path())
					fw.WatchList = append(fw.WatchList, ei.Path())
					continue
				}
			}
		}

		// add all other events to our set so we can periodically dispatch
		fw.DispatchSet.Add(ei.Path())
	}
}

//------------------------------------------------------------------------------
/**
 * Runs a command and prints its output to stdout of the calling process
 *
 * @param f = file name
 */
//------------------------------------------------------------------------------
func (fw *FileWatcher) dispatchDirectoryEvent(f string) {
	cmd := exec.Command(fw.Handler, f)
	if out, err := cmd.CombinedOutput(); err != nil {
		fmt.Println("Error dispatching: ", err)
	} else {
		fmt.Println(string(out))
	}
}
